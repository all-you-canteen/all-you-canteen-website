<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboards.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $data['nav_home'] = true;
        return view('dashboards.home')->withData($data);
    }

    public function canteens()
    {
        $data['nav_canteens'] = true;
        return view('dashboards.canteen')->withData($data);
    }

    public function anotherPlaces()
    {
        $data['nav_anotherplaces'] = true;
        return view('dashboards.anotherplace')->withData($data);
    }
}
