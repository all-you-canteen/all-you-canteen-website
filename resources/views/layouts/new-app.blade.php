<!DOCTYPE html>
<html lang="en">
<head>
    <title>Dashboard Admin</title>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE-edge">
    <meta name="viewport" content="width-device-width" , initial-scale="1.0">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@5.2.3/dist/css/bootstrap.min.css"
          integrity="sha384-rbsA2VBKQhggwzxH7pPCaAqO46MgnOM80zW1RWuH61DGLwZJEdK2Kadq2F9CUG65" crossorigin="anonymous">

    <!-- extra css -->
    @yield('extra-css')

    <!--Font google-->

    <link rel="preconnect" href="https://fonts.googleapis.com">
    <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
    <link href="https://fonts.googleapis.com/css2?family=Ubuntu:wght@400;500;700&display=swap" rel="stylesheet">

    <!--Css-->
    <link rel="stylesheet" href="{{ asset('css/new-style.css') }}">
</head>
<body>
<div class="Side-bar">
    <div class="sidebar">
        <div id="menu-button">
            <input type="checkbox" id="menu-checkbox">
            <label for="menu-checkbox" id="menu-label">
                <div id="hamburger">

                </div>
            </label>
        </div>
        <div class="header">
            <img src="{{ asset('assets/logo.png') }}" alt="" class="icon">
        </div>
        <div class="text-under">
            <a>
                Tel-U food recommendations
                {{ config('app.name', 'Laravel') }}
            </a>

        </div>

        <div class="main">
            <div class="list-item">
                <a href="{{ route('home') }}" style="text-decoration: none;">
                    <img src="{{ asset('assets/layout.png') }}" alt="" class="icon">
                    <span class="description">Dashboard</span>
                </a>
            </div>
            <div class="list-item">
                <a href="{{ route('foods') }}" style="text-decoration: none;">
                    <img src="{{ asset('assets/food-and-drink (1).png') }}" alt="" class="icon">
                    <span class="description">Food & Drink</span>
                </a>
            </div>
            <div class="list-item">
                <a href="{{ route('canteens') }}" style="text-decoration: none;">
                    <img src="{{ asset('assets/dining-table.png') }}" alt="" class="icon">
                    <span class="description">Kantin</span>
                </a>
            </div>
            <div class="list-item">
                <a href="{{ route('anotherplaces') }}" style="text-decoration: none;">
                    <img src="{{ asset('assets/placeholder.png') }}" alt="" class="icon">
                    <span class="description">Tempat Lainnya</span>
                </a>
            </div>
        </div>
    </div>

    <section id="interface">
        <div class="navigation">
            <div class="n1">
                <div class="search">
                    <i class="fa fa-search"></i>
                    <input type="text" placeholder="search">
                </div>
            </div>

            <div class="profile">
                <i class="fa fa-bell"></i>
                <a id="navbarDropdown" class="nav-link dropdown-toggle" href="#" role="button" data-bs-toggle="dropdown"
                   aria-haspopup="true" aria-expanded="false" v-pre>
                    {{ Auth::user()->name }}
                </a>

                <div class="dropdown-menu dropdown-menu-end" aria-labelledby="navbarDropdown">
                    <a class="dropdown-item" href="{{ route('logout') }}"
                       onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                        {{ __('Logout') }}
                    </a>

                    <form id="logout-form" action="{{ route('logout') }}" method="POST" class="d-none">
                        @csrf
                    </form>
                </div>
            </div>


        </div>

        <div class="container">
            <h3 class="i-name">
                @yield('title')
            </h3>

            @yield('content')
        </div>

    </section>

</div>
<script src="{{ asset('js/new-script.js') }}"></script>
<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.3.0-alpha2/dist/js/bootstrap.bundle.min.js"
        integrity="sha384-qKXV1j0HvMUeCBQ+QVp7JcfGl760yU08IQ+GpUo5hlbpg51QRiuqHAJz8+BrxE/N"
        crossorigin="anonymous"></script>

<!-- extra javascript -->
@yield('extra-javascript')

</body>

</html>
