@extends('layouts.app')

@section('title', 'Makanan dan Minuman')

@section('extra-css')
    <link rel="stylesheet" href="https://cdn.datatables.net/1.13.4/css/dataTables.bootstrap5.min.css">
@endsection

@section('extra-javascript')
    <script src="https://code.jquery.com/jquery-3.5.1.js"></script>
    <script src="https://cdn.datatables.net/1.13.4/js/jquery.dataTables.min.js"></script>
    <script src="https://cdn.datatables.net/1.13.4/js/dataTables.bootstrap5.min.js"></script>
    <script>
        $(document).ready(function () {
            $('#table-foods').DataTable();
        });
    </script>
@endsection

@section('content')
    <button class="btn btn-primary w-25 mb-4" data-bs-toggle="modal" data-bs-target="#foods-form">Tambah Makanan atau Minuman</button>

    <!-- Form Modal -->
    <div class="modal fade" id="foods-form" tabindex="-1" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <form action="{{ route('store.foods') }}" method="post">
                    {{ csrf_field() }}
                    <div class="modal-header">
                        <h1 class="modal-title fs-5" id="exampleModalLabel">Tambah Makanan</h1>
                        <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                    </div>
                    <div class="modal-body">
                        <div class="mb-3">
                            <label for="input-kode-makanan" class="form-label">Kode Makanan</label>
                            <input type="text" name="food_code" id="input-kode-makanan" class="form-control">
                            <div class="form-text">
                                Kode makanan berupa 6 digit karakter, contoh: TD44044
                                * Dua karakter awal diawali oleh kode kantin
                            </div>
                        </div>
                        <div class="mb-3">
                            <label for="input-nama-makanan" class="form-label">Nama Makanan atau Minuman</label>
                            <input type="text" name="food_name" id="input-nama-makanan" class="form-control">
                        </div>
                        <div class="mb-3">
                            <label for="input-harga" class="form-label">Harga</label>
                            <input type="number" name="food_price" id="input-harga" class="form-control">
                            <div class="form-text">
                                Harga dalam satuan Rupiah (Rp.)
                            </div>
                        </div>
                        <div class="mb-3">
                            <label for="input-quantity" class="form-label">Kuantitas</label>
                            <input type="number" name="food_quantity" id="input-quantity" class="form-control">
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Close</button>
                        <button type="submit" class="btn btn-primary">Save changes</button>
                    </div>
                </form>
            </div>
        </div>
    </div>

    <table id="table-foods" class="table table-striped" style="width:100%">
        <thead>
            <tr>
                @foreach($data['foods_columns'] as $column)
                    <th>{{ $column }}</th>
                @endforeach
            </tr>
        </thead>
        <tbody>
            @php $counter = 0 @endphp
            @foreach($data['foods'] as $food)
                <tr>
                    <td>{{ $counter += 1 }}</td>
                    <td>{{ $food['code'] }}</td>
                    <td>{{ $food['name'] }}</td>
                    <td>{{ $food[''] }}</td>
                    <td>{{ $food['quantity'] }}</td>
                    <td>{{ $food['price'] }}</td>
                </tr>
            @endforeach
        </tbody>
        <tfoot>
            <tr>
                @foreach($data['foods_columns'] as $column)
                    <th>{{ $column }}</th>
                @endforeach
            </tr>
        </tfoot>
    </table>
@endsection
