@extends('layouts.new-app')

@section('title', 'Makanan dan Minuman')

@section('extra-css')
    <link rel="stylesheet" href="https://cdn.datatables.net/1.13.4/css/dataTables.bootstrap5.min.css">
@endsection

@section('extra-javascript')
    <script src="https://code.jquery.com/jquery-3.5.1.js"></script>
    <script src="https://cdn.datatables.net/1.13.4/js/jquery.dataTables.min.js"></script>
    <script src="https://cdn.datatables.net/1.13.4/js/dataTables.bootstrap5.min.js"></script>
    <script src="https://use.fontawesome.com/ba689e9e78.js"></script>
    <script>
        $(document).ready(function () {
            $('#table-foods').DataTable();

            @if($errors->any())
            $("#error-toast").toast("show");
            @endif
        });

        $(document).on("click", ".tombol-hapus", function () {
            var foodId = $(this).data('id');
            var foodName = $(this).data('name');

            $("#delete-message-modal").text(`Yakin ingin menghapus ${foodName}?`);
            $("#delete-confirmation-modal form").attr('action', `{{ route('store.foods') }}/${foodId}`);

            // show modal
            $('#delete-confirmation-modal').modal('show');
        });

        $(document).on("click", ".tombol-edit", function () {
            var food = $(this).data('food');
            console.log(`food data is ${food.code}`)

            $("#form-title").text("Ubah Data Makanan");
            $("#input-kode-makanan").val(food.code);
            $("#input-nama-makanan").val(food.name);
            $("#input-harga").val(food.price);
            $("#input-quantity").val(food.quantity);
            $("#foods-form form").attr('action', `{{ route('store.foods') }}/${food.id}`);
            $("#foods-form form").append("<input name='_method' value='PUT' type='hidden'>")

            // show modal
            $('#foods-form').modal('show');
        });
    </script>
@endsection

@section('content')
    <div class="container">

        @if ($errors->any())
            {{-- error toast --}}
            @component('components.dashboard.toast', ['title' => "Errors"])
                @slot('message')
                    @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                @endslot
            @endcomponent
        @endif

        <!-- form modal for add data -->
        @component('components.dashboard.form-modal', [
            'route' => route('store.foods'),
            'method' => 'POST',
            'id' => 'foods-form',
        ])
            @slot('header')
                <h1 class="modal-title fs-5" id="form-title">Tambah Makanan</h1>
                <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
            @endslot
            @slot('body')
                <div class="mb-3">
                    <label for="input-kode-makanan" class="form-label">Kode Makanan</label>
                    <input type="text" name="food_code" id="input-kode-makanan" class="form-control">
                    <div class="form-text">
                        Kode makanan berupa 6 digit karakter, contoh: TD44044
                        * Dua karakter awal diawali oleh kode kantin
                    </div>
                </div>
                <div class="mb-3">
                    <label for="input-nama-makanan" class="form-label">Nama Makanan atau Minuman</label>
                    <input type="text" name="food_name" id="input-nama-makanan" class="form-control">
                </div>
                <div class="mb-3">
                    <label for="input-harga" class="form-label">Harga</label>
                    <input type="number" name="food_price" id="input-harga" class="form-control">
                    <div class="form-text">
                        Harga dalam satuan Rupiah (Rp.)
                    </div>
                </div>
                <div class="mb-3">
                    <label for="input-quantity" class="form-label">Kuantitas</label>
                    <input type="number" name="food_quantity" id="input-quantity" class="form-control">
                </div>
            @endslot
        @endcomponent

        <!-- form modal for delete confirmation -->
        @component('components.dashboard.form-modal', [
            'route' => route('delete.foods', ['id' => -1]),
            'method' => 'DELETE',
            'id' => "delete-confirmation-modal",
            'color' => "danger",
            'confirmation' => "Hapus Data"
        ])
            @slot('header')
                <h1 class="modal-title fs-5" id="delete-title-modal">Hapus Makanan</h1>
                <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
            @endslot
            @slot('body')
                <p id="delete-message-modal">Yakin akan menghapus makanan?</p>
            @endslot
        @endcomponent

        <div class="card">
            <div class="card-header d-flex justify-content-between">
                <p class="m-0 p-0">Makanan Gan</p>
                <button class="btn btn-primary btn-sm" data-bs-toggle="modal" data-bs-target="#foods-form">
                    Data Baru
                </button>
            </div>
            <div class="card-body">
                <table id="table-foods" class="table table-striped" style="width:100%">
                    <thead>
                    <tr>
                        @foreach($data['foods_columns'] as $column)
                            <th>{{ $column }}</th>
                        @endforeach
                        <th>Action</th>
                    </tr>
                    </thead>
                    <tbody>
                    @php $counter = 0 @endphp
                    @foreach($data['foods'] as $food)
                        <tr>
                            <td>{{ $counter += 1 }}</td>
                            <td>{{ $food['code'] }}</td>
                            <td>{{ $food['name'] }}</td>
                            <td>{{ $food[''] }}</td>
                            <td>{{ $food['quantity'] }}</td>
                            <td>{{ $food['price'] }}</td>
                            <td>
                                <!-- Delete Button -->
                                <button class="btn btn-danger tombol-hapus" data-id="{{ $food['id'] }}"
                                        data-name="{{ $food['name'] }}" data-bs-toggle="modal"
                                        data-bs-target="#delete-confirmation-modal">
                                    <i class="fa fa-trash"></i>
                                </button>

                                <!-- Edit Button -->
                                <button class="btn btn-warning tombol-edit" data-food='{{ json_encode($food) }}'
                                        data-bs-toggle="modal"
                                        data-bs-target="#edit-modal">
                                    <i class="fa fa-edit"></i>
                                </button>
                            </td>
                        </tr>
                    @endforeach
                    </tbody>
                    <tfoot>
                    <tr>
                        @foreach($data['foods_columns'] as $column)
                            <th>{{ $column }}</th>
                        @endforeach
                        <th>Action</th>
                    </tr>
                    </tfoot>
                </table>
            </div>
        </div>

    </div>
@endsection
