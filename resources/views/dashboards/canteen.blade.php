@extends('layouts.new-app')

@section('title', 'Kantin')

@section('extra-css')
    <link rel="stylesheet" href="https://cdn.datatables.net/1.13.4/css/dataTables.bootstrap5.min.css">
@endsection

@section('extra-javascript')
    <script src="https://code.jquery.com/jquery-3.5.1.js"></script>
    <script src="https://cdn.datatables.net/1.13.4/js/jquery.dataTables.min.js"></script>
    <script src="https://cdn.datatables.net/1.13.4/js/dataTables.bootstrap5.min.js"></script>
    <script src="https://use.fontawesome.com/ba689e9e78.js"></script>
    <script>
        $(document).ready(function () {
            // ganti id sesuaikan dengan table yang ada
            $('#table-foods').DataTable();
        });
    </script>
@endsection

@section('content')
    <div class="container">


    </div>
@endsection
